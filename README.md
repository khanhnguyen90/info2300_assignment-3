Bao Khanh Nguyen
StudentID:7779358


**Instruction**
Open project from BaoKhanhNguyen-Assignment1 folder.
This project was build base on HTML, CSS.
This project can run on any browsers such as IE, Firefox, Chrome,etc
This project can be modify on Notepad++, Visual Studio Code,...
Download Notepad++: https://notepad-plus-plus.org/downloads/
Download Visual Studio Code: https://code.visualstudio.com/download

---

## Lisences:


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation.
This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
Ref : https://www.gnu.org/licenses/fdl-1.3.html#addendum
---

